﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletHandler : MonoBehaviour {
    private Transform Target;
    public float speed = 5.0f;
	void Start () {
        Destroy(gameObject, 5);
    }
	
	void Update () {
        if (Target == null) return;
        
        Vector3 dir = Target.position - transform.position;
        float distThisFrame = speed * Time.deltaTime;
        transform.Translate(dir.normalized * distThisFrame, Space.World);
	}

    public void SetTarget(Transform enemy)
    {
        Target = enemy;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            GC.IncScore();
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
}
