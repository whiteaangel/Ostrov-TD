﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerHandler : MonoBehaviour {
    public GameObject TowerLevel1;
    private Transform SocketPos;
    public Animator TowerAnim;
    private const uint MinCost = 15;
    private void Start()
    {
        TowerAnim = GameObject.FindGameObjectWithTag("BuildTowerPanel").GetComponent<Animator>();
    }

    public void BuildTower()
    {   if (GC.Score > MinCost)
        {
            GC.DecScore(MinCost);
            Instantiate(TowerLevel1, SocketPos.position, Quaternion.identity, transform);
            CloseTowerMenu();
        }
    }

    public void CloseTowerMenu()
    {
        TowerAnim.SetTrigger("off");
    }
    
    public void SetSocketPos(Transform socket)
    {
        SocketPos = socket;
    }
}