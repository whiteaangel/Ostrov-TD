﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {
    enum WaveEnemy { Easy, Middle, Hard, StopWave };
    WaveEnemy CurrentWave = WaveEnemy.Easy;

    public GameObject Enemy;
    private float deltat = 5.0f;
    private float intervalt = 5.0f;
    private const int EasyMaxTime = 30, MiddleMaxTime = 60, HardMaxTime = 90;

	void Start () {
		
	}

    void Update () {
        if (CurrentWave == WaveEnemy.StopWave) return;
        WaveController();

        deltat += Time.deltaTime;
        if (deltat > intervalt)
        {
            deltat = 0.0f;
            Instantiate(Enemy, transform);
        }
	}

    void WaveController()
    {
        if (Time.time > EasyMaxTime && CurrentWave == WaveEnemy.Easy)
        {   
            CurrentWave = WaveEnemy.Middle;
        }
        else if (Time.time > MiddleMaxTime && CurrentWave == WaveEnemy.Middle)
        {
            CurrentWave = WaveEnemy.Hard;
        }
        else if (Time.time > HardMaxTime)
        {
            CurrentWave = WaveEnemy.StopWave;
        }
        IntervalController();
    }

    void IntervalController()
    {
        if (CurrentWave == WaveEnemy.Easy)
        {
            intervalt = 5.0f;
        } else if (CurrentWave == WaveEnemy.Middle)
        {
            intervalt = 4.0f;
        } else if (CurrentWave == WaveEnemy.Hard)
        {
            intervalt = 2.0f;
        }
    }
}
