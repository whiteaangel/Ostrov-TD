﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerShoot : MonoBehaviour {
    public GameObject Bullet;
    public Transform BulletSpawner;
    private const float TowerRadius = 10.0f;
    private const float ShootingInterval = 1.0f;
    private float DeltaTime = 1.0f;
    void Start () {
	}
	
	void Update () {
        DeltaTime += Time.deltaTime;
        if (DeltaTime > ShootingInterval)
        {
            Shoot(FoundNearestEnemy());
            DeltaTime = 0.0f;
        }
	}

    private void Shoot(GameObject enemy)
    {
        if (enemy == null) return;
        
        GameObject bullet = Instantiate(Bullet, BulletSpawner.position, BulletSpawner.rotation, BulletSpawner);
        bullet.GetComponent<BulletHandler>().SetTarget(enemy.transform);
    }

    GameObject FoundNearestEnemy()
    {
        float minDist = Mathf.Infinity;
        GameObject result = null;

        foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            float dist = Vector3.Distance(enemy.transform.position, BulletSpawner.position);
            if (dist < TowerRadius && dist < minDist)
            {
                minDist = dist;
                result = enemy;
            }
        }
        return result;
    }
}
