﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour {
    private uint Health = 3;
    NavMeshAgent Agent;
    void Start ()
    {
        Agent = GetComponent<NavMeshAgent>();
        Agent.destination = GameObject.FindGameObjectWithTag("Castle").transform.position;
    }
	
	void Update ()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Castle")
        {
            Debug.Log("YOU LOSE");
        }
    }
}
