﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GC : MonoBehaviour
{
    public enum GameState { Play, Pause, Win, Lose};
    public static GameState CurrentState = GameState.Play;
    public static uint Score = 20;
    public Text Pause;
    public static Text ScoreText;
    private void Start()
    {
        ScoreText = GameObject.FindGameObjectWithTag("Score").GetComponent<Text>();
        UpdateScoreText();
    }
    public void NewGame()
    {
        SceneManager.LoadScene("main");
    }
    public void LoadMenu()
    {
        SceneManager.LoadScene("menu");
    }
    public void ExitGame()
    {
        Application.Quit();
    }

    public void PauseGame()
    {
        if(CurrentState == GameState.Play)
        {
            Time.timeScale = 0.0f;
            CurrentState = GameState.Pause;
            Pause.text = "К игре";
        }
        else if(CurrentState == GameState.Pause)
        {
            Time.timeScale = 1.0f;
            CurrentState = GameState.Play;
            Pause.text = "Пауза";
        }
    }

    public static void IncScore()
    {
        Score += 10;
        UpdateScoreText();
    }

    public static void DecScore(uint value)
    {
        Score -= value;
        UpdateScoreText();
    }

    private static void UpdateScoreText()
    {
        ScoreText.text = "Cчет: " + Score.ToString();
    }
}
